<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'findingfranky');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'F}:|dnN.cKb8,918q:v48F!j=s5O2.uT?gM?NoC[3j>_G~TVPkBZCKovY,yL-LJY');
define('SECURE_AUTH_KEY',  '@_6eu$H//5ko%i#)=yuNHu5#Ml`Qh[20@s6`U,&Ik;zb>?pD#[fuGLF]4_l,A`}5');
define('LOGGED_IN_KEY',    'O~#x51#]30r3 DUssHg0mK/}@CA)~(KhRi=TNY9{R.[awOl5i*Duq~9Y|^^0/hYm');
define('NONCE_KEY',        '&6kf/n8T`_lj4kS1QL50e&7wR%=L3;3[;.`!81,19r2fq`^MN&g8Ic!Qw{gi&A7Y');
define('AUTH_SALT',        'b#JY@f7Wul9-5h[-1-||$Qt=80~X{u{]?@U!l$a0)Y{K@2&Ua=p>s!A+0wz]N+[p');
define('SECURE_AUTH_SALT', ';gwgzwpD7B)&yniR;$vx`e(}^89{P-k*4aqQw[.$2gC? }t?S(Zf0O&@q3A}BCUT');
define('LOGGED_IN_SALT',   'GA.C8JJ2qq >qL&T|w0Xpi&9_  Uz?(5l.xysl1bw]+)Nd:|O-fEf|nX?w(9Ph%8');
define('NONCE_SALT',       'd{KRPp6P)Uv/mq=|$B1T>C*A&uxRXmuNx)Dmf7O50^<{!(z=>h6?R)l#O)~ODE-o');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
