<?php
/**
 * User Franky Tips
 * return html
 */

global $current_user,$paged;
?>
<div class="tg-listingarea doc-franky-tips-wraper">
  <div class="tg-listing">
    <div class="tg-lists tg-franky-tips">
    	<div class="us-franky-tips">
    		<?php
    		global $current_user;
 			wp_get_current_user();
			$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
			$args = array(
			    'post_type' => 'franky_tips',
			    'post_status' => 'publish',
			    'posts_per_page' => 10,
			    'paged' => $paged,
			    'author' => $current_user->ID,
			);
			$tipsposts = new WP_Query( $args );?>
		
			<?php if ( $tipsposts->have_posts() ) : ?>

	        <div class="franky_tips_wrapper">
			<!-- pagination here -->
			    <!-- the loop -->
			    <?php while ( $tipsposts->have_posts() ) : $tipsposts->the_post(); ?>
	            	<div class="user_franky_tips_container">
			            <h2><?php echo get_the_title(); ?></h2>
			            <?php the_excerpt(); ?><a href="<?php the_permalink(); ?>" class="franky_tips_links">read more</a>
	            	</div>
			    <?php endwhile; ?>
			    <!-- end of the loop -->
			<!-- pagination here -->
	        </div>

			<?php if (function_exists("pagination")) {
			    pagination($tipsposts->max_num_pages);
			} ?>

			<?php wp_reset_postdata(); ?>

			<?php else : ?>
			<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>
    	</div>
    </div>
</div>
