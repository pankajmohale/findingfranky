<?php

/**

 * The header for our theme.

 *

 * Displays all of the <head> section and everything up till <div id="content">

 *

 * @package Doctor Directory

 */

?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

<meta charset="<?php bloginfo( 'charset' ); ?>">

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" >

<link rel="profile" href="http://gmpg.org/xfn/11">

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700" rel="stylesheet">

<?php wp_head();?>
<script type="text/javascript">
jQuery( document ).ready(function() {
    jQuery( ".doc-widgetheading .collapseToggle" ).click(function() {
    	//console.log(jQuery(this));
    	jQuery(this).parent().next('.doc-widgetcontent').slideToggle( "slow", function() {
	    // Animation complete.
		});
	  
	});
});
	
</script>
</head>

<body <?php body_class()?>>

<?php do_action('docdirect_init_headers');?>