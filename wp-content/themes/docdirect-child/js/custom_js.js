/* JS */
jQuery(document).ready(function(){
	jQuery('.prob_list h1').matchHeight({
	    byRow: true,
	});
	
	jQuery(this).on('touchmove', function (event) {
	    if (event.originalEvent.scale !== 1) {
	        event.preventDefault();
	        event.stopPropagation();
	    }
	});

	var loder_html	= '<div class="docdirect-site-wrap"><div class="docdirect-loader"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div>';

	//Do Process Account Settings
	jQuery(document).on('click','.preview_user_data',function(e){
		e.preventDefault();
		
		if( jQuery('.txt-professional').val() === 'txt-professional' ) {
			tinyMCE.triggerSave();
		}
		
		var $this 	= jQuery(this);
		var serialize_data	= jQuery('.do-account-setitngs').serialize();
		var dataString = serialize_data+'&action=docdirect_user_account_settings';
		
		jQuery.ajax({
			type: "POST",
			url: scripts_vars.ajaxurl,
			data: dataString,
			dataType:"json",
			success: function(response) {
				console.log(response);
				jQuery('body').find('.docdirect-site-wrap').remove();
				if( response.type == 'error' ) {
					jQuery.sticky(response.message, {classList: 'important', speed: 200, autoclose: 5000});
				} else{
					jQuery.sticky(response.message, {classList: 'success', speed: 200, autoclose: 5000,position: 'top-right',});
					var win = window.open(response.posturl, '_blank');
					if (win) {
					    //Browser has allowed it to be opened
					    win.focus();
					} else {
					    //Browser has blocked it
					    alert('Please allow popups for this website');
					}

				}
			}
		});
		return false;
	});
	
	// Reset form value
	jQuery('.doc-btnarea .tg-btn-reset').click(function(){
		jQuery(".search-result-form").trigger('reset');
	});
	
	if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Mac') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
		console.log('Safari on Mac detected, applying class...');
		jQuery('body').addClass('safari-mac'); // provide a class for the safari-mac specific css to filter with
	}
	
	if (navigator.userAgent.indexOf('Mac') >= 0) {
		jQuery('body').addClass('mac_os');
	}

});


//Init detail page Map Scripts
function custom_init_detail_map_script( _data_list ){
	var dir_latitude	 = scripts_vars.dir_latitude;
	var dir_longitude	= scripts_vars.dir_longitude;
	var dir_map_type	 = scripts_vars.dir_map_type;
	var dir_close_marker		  = scripts_vars.dir_close_marker;
	var dir_cluster_marker		= scripts_vars.dir_cluster_marker;
	var dir_map_marker			= scripts_vars.dir_map_marker;
	var dir_cluster_color		 = scripts_vars.dir_cluster_color;
	var dir_zoom				  = scripts_vars.dir_zoom;;
	var dir_map_scroll			= scripts_vars.dir_map_scroll;
	var gmap_norecod			  = scripts_vars.gmap_norecod;
	var map_styles			    = scripts_vars.map_styles;


	if( _data_list.status == 'found' ){
		var response_data	= _data_list.users_list;
	    if( typeof(response_data) != "undefined" && response_data !== null ) {
			var location_center = new google.maps.LatLng(response_data[0].latitude,response_data[0].longitude);
		} else {
				var location_center = new google.maps.LatLng(dir_latitude,dir_longitude);
		}
	} else{
		var location_center = new google.maps.LatLng(dir_latitude,dir_longitude);
	}

	
	if(dir_map_type == 'ROADMAP'){
		var map_id = google.maps.MapTypeId.ROADMAP;
	} else if(dir_map_type == 'SATELLITE'){
		var map_id = google.maps.MapTypeId.SATELLITE;
	} else if(dir_map_type == 'HYBRID'){
		var map_id = google.maps.MapTypeId.HYBRID;
	} else if(dir_map_type == 'TERRAIN'){
		var map_id = google.maps.MapTypeId.TERRAIN;
	} else {
		var map_id = google.maps.MapTypeId.ROADMAP;
	}
	
	var scrollwheel	= true;
	var lock		   = 'unlock';
	
	if( dir_map_scroll == 'false' ){
		scrollwheel	= false;
		lock		   = 'lock';
	}
	
	var mapOptions = {
		center: location_center,
		zoom: parseInt( dir_zoom ),
		mapTypeId: map_id,
		scaleControl: true,
		scrollwheel: scrollwheel,
		disableDefaultUI: true
	}
	
	var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
	
	var styles = docdirect_get_map_styles(map_styles);
	if(styles != ''){
		var styledMap = new google.maps.StyledMapType(styles, {name: 'Styled Map'});
		map.mapTypes.set('map_style', styledMap);
		map.setMapTypeId('map_style');
	}
		
	var bounds = new google.maps.LatLngBounds();
	
	//Zoom In
	if(  document.getElementById('doc-mapplus') ){ 
		 google.maps.event.addDomListener(document.getElementById('doc-mapplus'), 'click', function () {      
		   var current	= parseInt( map.getZoom(),10 );
		   current++;
		   if(current>20){
			   current=20;
		   }
		   map.setZoom(current);
		   jQuery(".infoBox").hide();
		});  
	}
	
	//Zoom Out
	if(  document.getElementById('doc-mapminus') ){ 
		google.maps.event.addDomListener(document.getElementById('doc-mapminus'), 'click', function () {      
			var current	= parseInt( map.getZoom(),10);
			current--;
			if(current<0){
				current=0;
			}
			map.setZoom(current);
			jQuery(".infoBox").hide();
		});  
	}
	
	//Lock Map
	if( document.getElementById('doc-lock') ){ 
		google.maps.event.addDomListener(document.getElementById('doc-lock'), 'click', function () {
			if(lock == 'lock'){
				map.setOptions({ 
						scrollwheel: true,
						draggable: true 
					}
				);
				
				jQuery("#doc-lock").html('<i class="fa fa-unlock-alt" aria-hidden="true"></i>');
				lock = 'unlock';
			}else if(lock == 'unlock'){
				map.setOptions({ 
						scrollwheel: false,
						draggable: false 
					}
				);
				
				jQuery("#doc-lock").html('<i class="fa fa-lock" aria-hidden="true"></i>');
				lock = 'lock';
			}
		});
	}

	//
	if( _data_list.status == 'found' && typeof(response_data) != "undefined" && response_data !== null ){
		jQuery('#gmap-noresult').html('').hide(); //Hide No Result Div
		var markers = new Array();
		var info_windows = new Array();
		
		for (var i=0; i < response_data.length; i++) {
			markers[i] = new google.maps.Marker({
				position: new google.maps.LatLng(response_data[i].latitude,response_data[i].longitude),
				map: map,
				icon: response_data[i].icon,
				title: response_data[i].title,
				animation: google.maps.Animation.DROP,
				visible: true
			});
		
			bounds.extend(markers[i].getPosition());
			
			var boxText = document.createElement("div");
			
			boxText.className = 'directory-detail';
			var innerHTML = "";
			boxText.innerHTML += response_data[i].html.content;
			
			var myOptions = {
				content: boxText,
				disableAutoPan: true,
				maxWidth: 0,
				alignBottom: true,
				pixelOffset: new google.maps.Size( 65, 15 ),
				zIndex: null,
				infoBoxClearance: new google.maps.Size( 1, 1 ),
				isHidden: false,
				closeBoxURL: dir_close_marker,
				pane: "floatPane",
				enableEventPropagation: false
			};
		
			var ib = new InfoBox( myOptions );
			custom_attachInfoBoxToMarker( map, markers[i], ib );
			ib.open(map,markers[i]);

		}
		
		map.fitBounds(bounds);
		
		var listener = google.maps.event.addListener(map, "idle", function() { 
			  if (map.getZoom() > 16) {
				  map.setZoom(parseInt( dir_zoom )); 
			  	  google.maps.event.removeListener(listener); 
			  }
		});

		/* Marker Clusters */
		var markerClustererOptions = {
			ignoreHidden: true,
			styles: [{
				textColor: scripts_vars.dir_cluster_color,
				url: scripts_vars.dir_cluster_marker,
				height: 48,
				width: 48
			}]
		};
		
		var markerClusterer = new MarkerClusterer( map, markers, markerClustererOptions );
	} else{
		jQuery('#gmap-noresult').html(gmap_norecod).show();
	}
}

//Assign Info window to marker
function custom_attachInfoBoxToMarker( map, marker, infoBox ){
	google.maps.event.addListener( marker, 'spider_click', function(){
		var scale = Math.pow( 2, map.getZoom() );
		var offsety = ( (100/scale) || 0 );
		var projection = map.getProjection();
		var markerPosition = marker.getPosition();
		var markerScreenPosition = projection.fromLatLngToPoint( markerPosition );
		var pointHalfScreenAbove = new google.maps.Point( markerScreenPosition.x, markerScreenPosition.y - offsety );
		var aboveMarkerLatLng = projection.fromPointToLatLng( pointHalfScreenAbove );
		map.setCenter( aboveMarkerLatLng );
		
		jQuery(".infoBox").hide();
		infoBox.open( map, marker );
		
		infoBox.addListener("domready", function() {
			jQuery('.tg-map-marker').on('click', '.add-to-fav', function (event) {
				event.preventDefault();
				
				var user_status	= scripts_vars.user_status;
				var fav_message	= scripts_vars.fav_message;
				var fav_nothing	= scripts_vars.fav_nothing;
	
				if( user_status == 'false' ){
					jQuery.sticky(fav_message, {classList: 'important', speed: 200, autoclose: 7000});
					return false;	
				}
				
				var _this	= jQuery(this);
				var wl_id	= _this.data('wl_id');
				_this.append('<i class="fa fa-refresh fa-spin"></i>');
				_this.addClass('loading');
				
				jQuery.ajax({
					type: "POST",
					url: scripts_vars.ajaxurl,
					data: 'wl_id='+wl_id+'&action=docdirect_update_wishlist',
					dataType: "json",
					success: function (response) {
						_this.removeClass('loading');
						_this.find('i.fa-spin').remove();
						jQuery('.login-message').show();
						if (response.type == 'success') {
							jQuery.sticky(response.message, {classList: 'success', speed: 200, autoclose: 5000,position: 'top-right',});
							_this.removeClass('tg-like add-to-fav');
							_this.addClass('tg-dislike');
						} else {
							jQuery.sticky(response.message, {classList: 'important', speed: 200, autoclose: 5000});
						}
					}
			   });
			});
		});
	});
}

// Send Review notification
jQuery(document).on('click','.user_notification',function(e){
  	// var comment_id= $(this).attr('value');
  	// action:'approve_notification',
    jQuery.ajax({
		type: "POST",
		url: scripts_vars.ajaxurl,
		data: {
                action:'approve_notification',
        },
		dataType: "json",
		success: function(response) {
			
			jQuery('body').find('.docdirect-site-wrap').remove();
			if( response.type == 'error' ) {
				jQuery.sticky(response.message, {classList: 'important', speed: 200, autoclose: 5000});
			} else{
				jQuery.sticky(response.message, {classList: 'success', speed: 200, autoclose: 5000});
			}
		}
   });
});