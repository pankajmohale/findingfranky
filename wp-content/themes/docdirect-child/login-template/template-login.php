<?php
/* 
	Template Name: Login
*/
get_header();

	$enable_resgistration	= '';
	$enable_login		= '';
	$captcha_settings		= '';
	$terms_link		= '';

	if(function_exists('fw_get_db_settings_option')) {
		$enable_resgistration = fw_get_db_settings_option('registration', $default_value = null);
		$enable_login = fw_get_db_settings_option('enable_login', $default_value = null);
		$captcha_settings = fw_get_db_settings_option('captcha_settings', $default_value = null);
		$terms_link = fw_get_db_settings_option('terms_link', $default_value = null);
	}
?>

	<?php while ( have_posts() ) : the_post();?>

			<div class="container">

				<div class="row">
					<div class="login-template">
						<?php 

							do_action('docdirect_prepare_section_wrapper_before');

								the_content();

							do_action('docdirect_prepare_section_wrapper_after');

						?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
						<div class="login-template" id="login-template">
							<?php if( $enable_login == 'enable' ) {
								if( apply_filters('docdirect_is_user_logged_in','check_user') === false ) {
									
									//Demo Ready
									$demo_username	= '';
									$demo_pass		= '';
									if( isset( $_SERVER["SERVER_NAME"] ) 
										&& $_SERVER["SERVER_NAME"] === 'themographics.com' ){
										$demo_username	= 'demo';
										$demo_pass		= 'demo';
									}
									
									if(function_exists('fw_get_db_settings_option')){
										$site_key = fw_get_db_settings_option('site_key');
									} else {
										$site_key = '';
									}
									
									?>
						            <form class="tg-form-modal tg-form-signin do-login-form custom-login-form">
						                <fieldset>
						                	<!-- <h1 class="login-registration-title"><?php esc_html_e('Log In', 'docdirect'); ?></h1> -->
						                    <div class="form-group">
						                        <input type="text" name="username" value="<?php echo esc_attr( $demo_username );?>" placeholder="<?php esc_html_e('Username/Email Address','docdirect_core');?>" class="form-control">
						                    </div>
						                    <div class="form-group">
						                        <input type="password" name="password" value="<?php echo esc_attr( $demo_pass );?>" class="form-control" placeholder="<?php esc_html_e('Password','docdirect_core');?>">
						                    </div>
						                    <div class="form-group tg-checkbox">
						                        <label>
						                            <input type="checkbox" class="form-control">
						                            <?php esc_html_e('Remember Me','docdirect_core');?>
						                        </label>
						                        <a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Lost Password' ) ) ); ?>" class="pull-right">
						                        	<i><?php esc_html_e('Forgot Password', 'docdirect_core'); ?></i>
						                            <i class="fa fa-question-circle"></i>
						                        </a>
						                        <!-- <a class="tg-forgot-password" href="javascript:;">
						                            <i><?php esc_html_e('Forgot Password', 'docdirect_core'); ?></i>
						                            <i class="fa fa-question-circle"></i>
						                        </a> -->
						                    </div>
						                    <?php 
											if( isset( $captcha_settings ) 
													&& $captcha_settings === 'enable' 
												) {
											?>
						                        <div class="domain-captcha">
						                            <div id="recaptcha_signin"></div>
						                        </div>
						                    <?php }?>
						                    <button class="tg-btn tg-btn-lg do-login-button"><?php esc_html_e('LOGIN now','docdirect_core');?></button>
						                </fieldset>
						            </form>
						         <?php }
								} else{?>
								<div class="tg-form-modal">
									<p class="alert alert-info theme-notification"><?php esc_html_e('Sign In is disabled by administrator','docdirect_core');?></p>
								</div>
							<?php }?>
						</div>
					</div>
				</div>

			</div>

		<?php 

		endwhile;
		?>
<?php get_footer(); ?>