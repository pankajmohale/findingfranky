<?php
/* 
	Template Name: Resgistration
*/
get_header();

	$enable_resgistration	= '';
	$enable_login		= '';
	$captcha_settings		= '';
	$terms_link		= '';

	if(function_exists('fw_get_db_settings_option')) {
		$enable_resgistration = fw_get_db_settings_option('registration', $default_value = null);
		$enable_login = fw_get_db_settings_option('enable_login', $default_value = null);
		$captcha_settings = fw_get_db_settings_option('captcha_settings', $default_value = null);
		$terms_link = fw_get_db_settings_option('terms_link', $default_value = null);
	}
?>

	<?php while ( have_posts() ) : the_post();?>

			<div class="container">

				<div class="row">
					<div class="join-template">
						<?php 

							do_action('docdirect_prepare_section_wrapper_before');

								the_content();

							do_action('docdirect_prepare_section_wrapper_after');

						?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2">
						<div class="registration-template custom-registration-section" id="registration-template">
							<h1 class="join-title login-registration-title"><?php esc_html_e('Join', 'docdirect'); ?></h1>
							<span class="border-bottom"></span>
							<?php 
							if( $enable_resgistration == 'enable') {
								if( apply_filters('docdirect_is_user_logged_in','check_user') === false ) {?>
								<?php echo do_shortcode('[gravityform id="5" title="false" description="false" ajax="true"]'); ?>
                            <?php }
							} else{?>
								<div class="tg-form-modal">
									<p class="alert alert-info theme-notification"><?php esc_html_e('Registration is disabled by administrator','docdirect_core');?></p>
								</div>
							<?php }?>
						</div>
					</div>
				</div>

			</div>

		<?php 

		endwhile;
		?>
<?php get_footer(); ?>