<?php
/*
Template Name: Home Page
*/
get_header();
?>
<div class="full-width">
	<div class="container">
	    <div class="col-md-12">
	        <?php
	        while(have_posts()) : the_post();
	            the_content();
	        endwhile;
	        ?>
	    </div>		
	</div>
	<div class="home-full-section">
		<div class="container">
			<div class="row">
				<?php echo get_field( "full_width_content" ); ?>
			</div>
		</div>
	</div>
	<div class="home-content-section">
		<div class="container">
			<div class="row">
				<?php echo get_field( "normal_content" ); ?>
			</div>
		</div>
	</div>
	<div class="home-before-footer-section">
		<div class="container">
			<div class="row">
				<?php echo get_field( "content_before_footer" ); ?>
			</div>
		</div>
	</div>
</div>
<?php
get_footer();
?>