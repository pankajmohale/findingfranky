<?php
/* 
	Template Name: Left Sidebar Template
*/
get_header();
?>
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 aside sidebar-section" id="sidebar">
				<div class="aside sidebar-section" id="sidebar">
					<aside id="tg-sidebar" class="tg-sidebar tg-haslayout ">
						<?php //get_sidebar();?>
							<?php
							$tipsargs = array(
							    'post_type' => 'franky_tips',
							    'post_status' => 'publish',
							    'posts_per_page' => -1,
							    'paged' => $paged,
							);
							$tipsquery = new WP_Query( $tipsargs ); ?>

					    	<?php 
							if ( wp_is_mobile() ) { ?>
								<?php //dynamic_sidebar( 'tips-mobile' ); ?>
								<?php
									$poctcount = 1; ?>
										<div class="tg-widget tips-normal">
											<h3>Recent Tips</h3>
											<?php if ( $tipsquery->have_posts() ) : ?>

													<!-- pagination here -->
													<ul >
														<!-- the loop -->
														<?php while ( $tipsquery->have_posts() ) : $tipsquery->the_post(); ?>
																<li>
																	<a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a>
																	<span class="post-date"><?php echo get_the_date('F j, Y'); ?></span>
																</li>
																
																
															<?php if ( $poctcount == 6 ) { ?>
																</ul><ul class="collapse" id="collapseExample">
															<?php } ?>
															<?php $poctcount++; ?>
														<?php endwhile; ?>
														<!-- end of the loop -->
													</ul>

													<!-- pagination here -->

													<?php wp_reset_postdata(); ?>

												<?php else : ?>
													<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
												<?php endif; ?>
										</div>
										<?php
								if ($tipsquery->post_count > 6) { ?> 
									<button class="btn btn-primary expand-tips-btn" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
									<i class="fa fa-plus" aria-hidden="true"></i>
									</button>
								<?php } ?>


							<?php } else { ?>
							
								<?php //dynamic_sidebar( 'tips-desktop' ); ?>
								<?php
								if ($tipsquery->post_count <= 6) { ?>
									
									<div class="tg-widget tips-normal">
										<h3>Recent Tips</h3>
										<?php if ( $tipsquery->have_posts() ) : ?>

												<!-- pagination here -->
												<ul >
													<!-- the loop -->
													<?php while ( $tipsquery->have_posts() ) : $tipsquery->the_post(); ?>
														<li>
															<a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a>
															<span class="post-date"><?php echo get_the_date('F j, Y'); ?></span>
														</li>
															
													<?php endwhile; ?>
													<!-- end of the loop -->
												</ul>

												<!-- pagination here -->

												<?php wp_reset_postdata(); ?>

											<?php else : ?>
												<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
											<?php endif; ?>
									</div>


								<?php } else { ?>

									<div class="tg-widget tips-desktop">
										<h3>Recent Tips</h3>
										<?php if ( $tipsquery->have_posts() ) : ?>

											<!-- pagination here -->
											<ul>
												<!-- the loop -->
												<?php while ( $tipsquery->have_posts() ) : $tipsquery->the_post(); ?>
													<li>
														<a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a>
														<span class="post-date"><?php echo get_the_date('F j, Y'); ?></span>
													</li>
														
												<?php endwhile; ?>
												<!-- end of the loop -->
											</ul>

											<!-- pagination here -->

											<?php wp_reset_postdata(); ?>

										<?php else : ?>
											<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
										<?php endif; ?>
									</div>
								<?php } ?>
							<?php }	?>							

					</aside>
				</div>
			</div>
			<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
				<div class="franky-add-tips">
					<?php
						while ( have_posts() ) : the_post();
							the_content();
						endwhile; // End of the loop.
					?>					
				</div>
			</div>
		</div>
	</div>
<?php
get_footer();
?>
