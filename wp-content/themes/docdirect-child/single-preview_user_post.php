<?php
/**
 * The template for displaying all single posts.
 *
 * @package Doctor Directory
 */
get_header();
global $current_user;
$directory_type	= $current_user->directory_type;

if(function_exists('fw_get_db_settings_option')) {
	$dir_map_marker    = fw_get_db_post_option($directory_type, 'dir_map_marker', true);
	$dir_map_marker_default = fw_get_db_settings_option('dir_map_marker');	
}

if( !empty( $dir_map_marker['url'] ) ){
	$dir_map_marker  = $dir_map_marker['url'];
} else{
	if( !empty( $dir_map_marker_default['url'] ) ){
		$dir_map_marker	 = $dir_map_marker_default['url'];
	} else{
		$dir_map_marker	 	   = get_template_directory_uri().'/images/map-marker.png';
	}
}

$current_post_id = get_the_ID();
?>
<div class="container">
 	<div class="row">
	    <div class="tg-userdetail custom-user_info <?php echo sanitize_html_class( $apointmentClass );?>">
	      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	        <aside id="tg-sidebar" class="tg-sidebar">
	          <div class="tg-widget tg-widgetuserdetail">
	            <figure class="tg-userimg"> 
	            	<?php 
		            	$user_id = $current_user->ID;
						$key = 'userprofile_media';
						$single = true;
						$user_profile = get_user_meta( $user_id, $key, $single );
	            	?>

	            	<?php if( !empty( $user_profile ) ){?>
	            		<img src="<?php echo wp_get_attachment_url( $user_profile ); ?>" alt="<?php echo esc_attr( $current_user->first_name.' '.$current_user->last_name );?>">
					<?php } else { ?>
	            		<img src="<?php echo get_stylesheet_directory_uri().'/img/user.jpg'; ?>" alt="<?php echo esc_attr( $current_user->first_name.' '.$current_user->last_name );?>">					
					<?php } ?>
	              <figcaption>
	                <ul class="tg-featureverified">
					  <?php if( isset( $featured_string ) && $featured_string > $current_string ){?>
	                        <li class="tg-featuresicon"><a href="javascript:;"><i class="fa fa-bolt"></i><span><?php esc_html_e('featured','docdirect');?></span></a></li>
	                  <?php }?>
	                  <?php docdirect_get_verified_tag(true,$current_author_profile->ID,'simple');?>
	                </ul>
	              </figcaption>
	            </figure>
	          </div>
	        </aside>
	      </div>
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		        <div class="tg-haslayout custom-user-content">
		          	<div class="tg-userbanner-content">
		          		<?php 
			          		$first_name = get_post_meta( $current_post_id, 'first_name', true );
			          		$last_name = get_post_meta( $current_post_id, 'last_name', true );
			          		$description = get_post_meta( $current_post_id, 'description', true );
		          		?>
		                <h1><?php echo $first_name.' '.$last_name; ?></h1>
		          	</div>
		          	<div class="tg-aboutuser">
		            	<?php if( !empty( $description ) ) {?>
		              	<div class="tg-description">
		                	<?php echo wpautop( $description );?>
		              	</div>
		            	<?php }?>
		          	</div>
		          	<?php 
		          		$longitude = get_post_meta( $current_post_id, 'longitude', true );
		          		$latitude = get_post_meta( $current_post_id, 'latitude', true );
		          		$user_address = get_post_meta( $current_post_id, 'user_address', true );
		          		$location = get_post_meta( $current_post_id, 'location', true );
		          		$nickname = get_post_meta( $current_post_id, 'nickname', true );
	          		?>
		          	<?php if( !empty( $longitude ) && !empty( $latitude ) ) {?>
						<div class="tg-section-map">
							<div id="map_canvas" class="tg-location-map tg-haslayout"></div>
							<?php do_action('docdirect_map_controls');?>
							<?php
								$directories = array();
								$directories_array	= array();
								$directories['status']	= 'found';
								$directories_array['latitude']	= $latitude;
								$directories_array['longitude']	= $longitude;
								$directories_array['title']	= $nickname;
								$directories_array['name']	 = $first_name.' '.$last_name;
								$directories_array['email']	 		 = $current_user->user_email;
								$directories_array['phone_number']	 = $phone_number;
								$directories_array['address']	 = $user_address;
								$directories_array['group']	= '';
								$directories_array['icon']	 	   = $dir_map_marker;
								$avatar = apply_filters(
									'docdirect_get_user_avatar_filter',
									 docdirect_get_user_avatar(array('width'=>150,'height'=>150), $current_user->ID),
									 array('width'=>150,'height'=>150) //size width,height
								);

								$infoBox	= '<div class="tg-mapmarker">';
								$infoBox	.= '<figure><img width="60" heigt="60" src="'.esc_url( $avatar ).'" alt="'.esc_attr__('User','docdirect').'"></figure>';
								$infoBox	.= '<div class="tg-mapmarkercontent">';
								$infoBox	.= '<h3><a href="'.get_author_posts_url($current_user->ID).'">'.$directories_array['name'].'</a></h3>';

								if( !empty( $current_user->tagline ) ) {
									$infoBox	.= '<span>'.$current_user->tagline.'</span>';
								}


								$infoBox	.= '<ul class="tg-likestars">';

								if( isset( $reviews_switch ) && $reviews_switch === 'enable' && !empty( $review_data )){
									$infoBox	.= '<li>'.docdirect_get_rating_stars($review_data,'return','hide').'</li>';
								}
								$infoBox	.= '<li>'.docdirect_get_wishlist_button($current_user->ID,false).'</li>';
								$infoBox	.= '<li>'.docdirect_get_user_views($current_user->ID).'&nbsp;'.esc_html__('view(s)','docdirect').'</li>';

								$infoBox	.= '</ul>';
								$infoBox	.= '</div>';
																					
								$directories_array['html']['content']	= $infoBox;
								$directories['users_list'][]	= $directories_array;								
							?>
							<script>
								jQuery(document).ready(function() {
									custom_init_detail_map_script(<?php echo json_encode( $directories );?>);
								});	
							</script>
						</div> 
						<?php }?>
						<div class="user-more-description">
							<?php 
				          		$counsellor_statements = get_post_meta( $current_post_id, 'counsellor_statements', true );
			          		?>
							<?php if( !empty( $counsellor_statements ) ){?>
								<div class="professional-statements">
							    	<?php echo do_shortcode( nl2br( $counsellor_statements));?>
							    </div>
							<?php }?>          		
						</div>

						<!--Video URL-->
						<?php 
			          		$video_url = get_post_meta( $current_post_id, 'video_link', true );
		          		?>
						<?php if( isset( $video_url ) && !empty( $video_url ) ) { ?>
						  <div class="tg-presentationvideo">
						    <?php
								$height = 200;
								$width  = 368;
								$post_video = $video_url;
								$url = parse_url( $post_video );
								if ($url['host'] == $_SERVER["SERVER_NAME"]) {
									echo '<div class="video">';
									echo do_shortcode('[video width="' . $width . '" height="' . $height . '" src="' . $post_video . '"][/video]');
									echo '</div>';
								} else {

									if ($url['host'] == 'vimeo.com' || $url['host'] == 'player.vimeo.com') {
										echo '<div class="video">';
										$content_exp = explode("/", $post_video);
										$content_vimo = array_pop($content_exp);
										echo '<iframe width="' . $width . '" height="' . $height . '" src="https://player.vimeo.com/video/' . $content_vimo . '" 
						></iframe>';
										echo '</div>';
									} elseif ($url['host'] == 'soundcloud.com') {
										$video = wp_oembed_get($post_video, array('height' => $height));
										$search = array('webkitallowfullscreen', 'mozallowfullscreen', 'frameborder="no"', 'scrolling="no"');
										echo '<div class="audio">';
										$video = str_replace($search, '', $video);
										echo str_replace('&', '&amp;', $video);
										echo '</div>';
									} else {
										echo '<div class="video">';
										$content = str_replace(array('watch?v=', 'http://www.dailymotion.com/'), array('embed/', '//www.dailymotion.com/embed/'), $post_video);
										echo '<iframe width="' . $width . '" height="' . $height . '" src="' . $content . '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
										echo '</div>';
									}
								}
							?>
						  </div>
						  <?php }?>
						<?php 
			          		$treatment_methods = get_post_meta( $current_post_id, 'treatment_methods', true );
		          		?>
						<?php if( !empty( $treatment_methods ) ){?>
							<div class="tg-treatment-methods">
								<div class="tg-userheading">
									<h2><?php esc_html_e('Treatment Methods','docdirect');?></h2>
								</div>
								<div class="professional-statements">
									<?php echo do_shortcode( nl2br( $treatment_methods));?>
								</div>
							</div>
						<?php }?>
			      	<div class="user-qualities">
						<div class="row">
						<?php 
			          		$focus_areas = get_post_meta( $current_post_id, 'focus_areas', true );
		          		?>
						<?php if( !empty( $focus_areas ) ){?>
							<div class="col-xs-12 col-sm-6">
								<div class="tg-focus-areas">
									<div class="tg-userheading">
										<h2><?php esc_html_e('Focus Areas','docdirect');?></h2>
									</div>
									<div class="focus-areas">
										<?php echo do_shortcode( $focus_areas);?>
									</div>
								</div>
							</div>
						<?php }?>
			        	<?php 
							$term_list = wp_get_post_terms($current_post_id, 'preview_pecialities', array("fields" => "names"));
							// print_r($term_list);
			        	?>
						<?php if( !empty( $term_list ) ) {?>
							<div class="col-xs-12 col-sm-6">
							  <!--Specialities-->
							      <div class="tg-honourawards tg-listview-v3 user-section-style sp-icon-wrap">
							        <div class="tg-userheading">
							          <h2><?php esc_html_e('Aditional Specialities / Services','docdirect');?></h2>
							        </div>
							        <div class="tg-doctor-profile">
							              <ul class="tg-tags">
							                <?php
							                  foreach ($term_list as $term_key => $term_value) { ?>
							                  	<li>
							                    	<div class="specialities-wrap">
							                    		<span class="icon-sp">
							                    			<i class="fa fa-check-square-o" aria-hidden="true"></i>
							                    		</span>                    		
							                   			<span><?php echo $term_value;?></span>
							                    	</div>
							                    </li>
							                <?php  }
							                ?>
							              </ul>
							          </div>
							      </div>
							</div>
							<?php }?>
						</div>
			        </div>
			        <?php 
		          		$description_of_practice = get_post_meta( $current_post_id, 'description_of_practice', true );
	          		?>
			        <?php if( !empty( $description_of_practice ) ){?>
		          		<div class="tg-description_of_practice">
							<div class="tg-userheading">
								<h2><?php esc_html_e('Description of Practice','docdirect');?></h2>
							</div>
								<div class="professional-statements">
									<?php echo do_shortcode( nl2br( $description_of_practice));?>
								</div>
						</div>
					<?php }?>

					<?php 
		          		$qualifications_experience = get_post_meta( $current_post_id, 'qualifications_experience', true );
		          		$information_of_fees = get_post_meta( $current_post_id, 'information_of_fees', true );
	          		?>
					<div class="qualification-info">
			          	<div class="row">
					        <?php if( !empty( $qualifications_experience ) ){?>
			          			<div class="col-xs-12 col-sm-6">
					          		<div class="tg-qualifications_experience">
										<div class="tg-userheading">
											<h2><?php esc_html_e('Qualifications / Experience','docdirect');?></h2>
										</div>
											<div class="professional-statements">
												<?php echo do_shortcode( $qualifications_experience);?>
											</div>
									</div>
			          			</div>
							<?php }?>
		          			<?php if( !empty( $information_of_fees ) ){?>
				          		<div class="col-xs-12 col-sm-6">
					          		<div class="tg-fees_information">
										<div class="tg-userheading">
											<h2><?php esc_html_e('Information on fees','docdirect');?></h2>
										</div>
										<div class="professional-statements">
											<?php echo do_shortcode( $information_of_fees);?>
										</div>
									</div>
				          		</div>
							<?php }?>
			          	</div>
			        </div>

			        <?php 
			        	// Contact Info
		          		$user_address = get_post_meta( $current_post_id, 'user_address', true );
		          		$fax = get_post_meta( $current_post_id, 'fax', true );
		          		$phone_number = get_post_meta( $current_post_id, 'phone_number', true );
		          		$user_url = get_post_meta( $current_post_id, 'user_url', true );
	          		?>
			        <div class="counsellor_info">
		            	<div class="row">
		            		<div class="col-sm-7">
		            			<div class="tg-userschedule user_info">
			            			<div class="tg-userheading">
			            				<h2><?php esc_html_e('Contact','docdirect');?></h2>
			            			</div>
				        			<ul class="tg-doccontactinfo">
						                <?php if( !empty( $user_address ) ) {?>
						                	<li> <i class="fa fa-map-marker"></i> <address><?php echo esc_attr( $user_address );?></address> </li>
						                <?php }?>
						                <?php if( !empty( $current_user->user_email ) ) {?>
						                    <li><i class="fa fa-envelope-o"></i><a href="mailto:<?php echo esc_attr( $current_user->user_email );?>?subject:<?php esc_html_e('Hello','docdirect');?>"><?php echo esc_attr( $current_user->user_email );?></a></li>
						                <?php }?>
						                <?php if( !empty( $phone_number ) ) {?>
						                	<li> <i class="fa fa-phone"></i> <span><?php echo esc_attr( $phone_number );?></span> </li>
						                <?php }?>
						                <?php if( !empty( $fax ) ) {?>
						                	<li><i class="fa fa-fax"></i> <span><?php echo esc_attr( $fax );?></span> </li>
						                <?php }?>
						                  <?php if( !empty( $current_user->skype ) ) {?> 
						                	<li><i class="fa fa-skype"></i><span><?php echo esc_attr( $current_user->skype );?></span></li>
						                <?php }?>
						              
						                <?php if( !empty( $user_url ) ) {?>
						                    <li><i class="fa fa-link"></i><a href="<?php echo $user_url;?>" target="_blank"><?php echo docdirect_parse_url( $user_url);?></a></li>
						                <?php }?>
						              </ul>
		            			</div>
		            		</div>
		            		<?php 
					        	// Contact Info
				          		$user_url = get_post_meta( $current_post_id, 'user_url', true );
			          		?>
		            		<div class="col-sm-5">
		            			<?php
								if( apply_filters('docdirect_is_setting_enabled',$current_author_profile->ID,'schedules' ) === true ){
									?>
									<div class="tg-userschedule">
										<div class="tg-userheading"><h2><?php esc_html_e('Practice Hours','docdirect');?></h2></div>
									<ul>

									<?php 
										$week_array	= docdirect_get_week_array();
										// $db_schedules	= array();
										// if( isset( $current_author_profile->schedules ) && !empty( $current_author_profile->schedules ) ){
										// 	$db_schedules	= $current_author_profile->schedules;
										// }

										//Time format
										if( isset( $schedule_time_format ) && $schedule_time_format === '24hour' ){
											$time_format	= 'G:i';
										} else{
											$time_format	= 'g:i A';
										}

										$date_prefix	= date('D');
										if( isset( $week_array ) && !empty( $week_array ) ) {
										foreach( $week_array as $key => $value ){
											$start_time_formate	 = '';
											$end_time_formate	   = '';
											$start_time  = get_post_meta( $current_post_id, $key.'_start', true );
											$end_time	= get_post_meta( $current_post_id, $key.'_end', true );

											if( !empty( $start_time ) ){
												$start_time_formate	= date( $time_format, strtotime( $start_time ) );
											}


											if( isset( $end_time ) && !empty( $end_time ) ){ 
												$end_time_formate	= date( $time_format, strtotime( $end_time ) );
												$end_time_formate	= docdirect_date_24midnight($time_format,strtotime( $end_time ));
											}

											//Active day
											$active	= '';
											if( strtolower( $date_prefix ) == $key ){
												$active	= 'current';
											}

											//
											if( !empty( $start_time_formate ) && $end_time_formate ) {
												$data_key	= $start_time_formate.' - '.$end_time_formate;
											} else if( !empty( $start_time_formate ) ){
												$data_key	= $start_time_formate;
											} else if( !empty( $end_time_formate ) ){
												$data_key	= $end_time_formate;
											} else{
												$data_key	= esc_html__('Closed','docdirect');
											}
										?>
										<li class="<?php echo sanitize_html_class( $active );?>"><a href="javascript:;" data-type="<?php echo esc_attr( $data_key );?>"><span><?php echo esc_attr( $value );?></span><em><?php echo esc_attr( $data_key );?></em></a></li>

									<?php }}?>
								</ul>
								</div>
				             	<?php }?>
		            		</div>
		            	</div>
		            </div>

		      	</div>
	  		</div>
	  	</div>
	</div>
</div>
<?php get_footer(); ?>
