<?php

/**
 * Theme functions file
 */

/**
 * Enqueue parent theme styles first
 * Replaces previous method using @import
 * <http://codex.wordpress.org/Child_Themes>
 */



function docdirect_child_theme_enqueue_styles() {

    $parent_style = 'docdirect_theme_style';

  	wp_enqueue_style( 'docdirect_child_style',

        get_stylesheet_directory_uri() . '/style.css',

        array( 'bootstrap.min', $parent_style)

    );

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    
    wp_register_style( 'second-style', get_stylesheet_directory_uri() .'/css/second-style.css', array(), '21');
    wp_enqueue_style( 'second-style' );
    
    if ( is_single()) {
    /** Call landing-page-template-one enqueue */
      wp_enqueue_script('google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBuU_0_uLMnFM-2oWod_fzC0atPZj7dHlU&sensor=false');
      wp_enqueue_script('google-jsapi','https://www.google.com/jsapi');

      wp_enqueue_script('docdirect_infobox', get_template_directory_uri() . '/js/map/infobox.js', array(), true);
      wp_enqueue_script('markerclusterer', get_template_directory_uri() . '/js/map/markerclusterer.min.js', array(), true);
    }


    
    wp_register_script( 'matchheight_js', get_stylesheet_directory_uri() . '/js/jquery.matchHeight-min.js', array('jquery'), '1.0', true);
    wp_enqueue_script( 'matchheight_js' );

    wp_register_script( 'custom_js', get_stylesheet_directory_uri() . '/js/custom_js.js', array(), '1.0.0', true);
    wp_enqueue_script( 'custom_js' );


}



add_action( 'wp_enqueue_scripts', 'docdirect_child_theme_enqueue_styles' );



/* ******************************************** Inclue File from child ******************************************** */

require_once ( get_stylesheet_directory() . '/inc/footers/class-footers.php');

require_once ( get_stylesheet_directory() . '/inc/headers/class-headers.php');

require_once ( get_stylesheet_directory() . '/inc/subheaders/class-subheaders.php');





/**
 * Add a sidebar.
 */

function wpdocs_theme_slug_widgets_init() {

    register_sidebar(array(

		'name' => esc_html__('Footer Column 4', 'docdirect'),

		'id' => 'footer-column-4',

		'description' => '',

		'before_widget' => '<div id="%1$s" class="%2$s">',

		'after_widget' => '</div>',

		'before_title' => '<div class="tg-heading-border tg-small"><h4>',

		'after_title' => '</h4></div>',

	));



	register_sidebar(array(

		'name' => esc_html__('Footer Column 5', 'docdirect'),

		'id' => 'footer-column-5',

		'description' => '',

		'before_widget' => '<div id="%1$s" class="%2$s">',

		'after_widget' => '</div>',

		'before_title' => '<div class="tg-heading-border tg-small"><h4>',

		'after_title' => '</h4></div>',

	));
	



}

add_action( 'widgets_init', 'wpdocs_theme_slug_widgets_init' );

/**
 * Add a Custom post type.
 */

function create_posttype_problem() {
  register_post_type( 'problems',
    array(
      'labels' => array(
        'name' => __( 'WHAT’S THE PROBLEMS' ),
        'singular_name' => __( 'WHAT’S THE PROBLEM' )
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'problems'),
      'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt','page-attributes') ,
    )
  );
}
add_action( 'init', 'create_posttype_problem' );

function create_posttype_theydo() {
  register_post_type( 'theydo',
    array(
      'labels' => array(
        'name' => __( 'WHAT DO THEY DO’S' ),
        'singular_name' => __( 'WHAT DO THEY DO' )
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'theydo'),
      'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt','page-attributes') ,
    )
  );
}
add_action( 'init', 'create_posttype_theydo' );


add_action( 'init', 'custom_preview_posttype' );
/**
 * Register a Preview post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function custom_preview_posttype() {
    $labels = array(
        'name'               => _x( 'Preview User Post', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Preview User Post', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Preview User Posts', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Preview User Post', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New', 'Preview User Post', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New Preview User Post', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New Preview User Post', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit Preview User Post', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View Preview User Post', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All Preview User Posts', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search Preview User Posts', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent Preview User Posts:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No Preview User Posts found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No Preview User Posts found in Trash.', 'your-plugin-textdomain' )
    );

    $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'preview_user_post' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
    );

    register_post_type( 'preview_user_post', $args );

    // Add new taxonomy, make it hierarchical (like categories)
    $specialities = array(
      'name'              => _x( 'Specialities', 'taxonomy general name', 'textdomain' ),
      'singular_name'     => _x( 'Specialities', 'taxonomy singular name', 'textdomain' ),
      'search_items'      => __( 'Search Specialities', 'textdomain' ),
      'all_items'         => __( 'All Specialities', 'textdomain' ),
      'parent_item'       => __( 'Parent Specialitie', 'textdomain' ),
      'parent_item_colon' => __( 'Parent Specialitie:', 'textdomain' ),
      'edit_item'         => __( 'Edit Specialitie', 'textdomain' ),
      'update_item'       => __( 'Update Specialitie', 'textdomain' ),
      'add_new_item'      => __( 'Add New Specialitie', 'textdomain' ),
      'new_item_name'     => __( 'New Specialitie Name', 'textdomain' ),
      'menu_name'         => __( 'Specialitie', 'textdomain' ),
    );

    $args = array(
      'hierarchical'      => true,
      'labels'            => $specialities,
      'show_ui'           => true,
      'show_admin_column' => true,
      'query_var'         => true,
      'rewrite'           => array( 'slug' => 'preview_pecialities' ),
    );

    register_taxonomy( 'preview_pecialities', array( 'preview_user_post' ), $args );
}

function revcon_change_cat_object() {
    global $wp_taxonomies;
    $labels = &$wp_taxonomies['insurance']->labels;
    $labels->name = 'problems';
    $labels->singular_name = 'problems';
    $labels->add_new = 'Add problem';
    $labels->add_new_item = 'Add problem';
    $labels->edit_item = 'Edit problems';
    $labels->new_item = 'problems';
    $labels->view_item = 'View problem';
    $labels->search_items = 'Search problems';
    $labels->not_found = 'No problems found';
    $labels->not_found_in_trash = 'No problems found in Trash';
    $labels->all_items = 'All problems';
    $labels->menu_name = 'Problems';
    $labels->name_admin_bar = 'Problems';
}
add_action( 'init', 'revcon_change_cat_object' );

add_action( 'init', 'register_post_type_init' );
/**
 * Register a book post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function register_post_type_init() {
    $labels = array(
        'name'               => _x( 'Franky’s Tips', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Franky’s Tip', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Franky’s Tips', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Franky’s Tip', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New', 'Franky’s Tip', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New Franky’s Tip', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New Franky’s Tip', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit Franky’s Tip', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View Franky’s Tip', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All Franky’s Tips', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search Franky’s Tips', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent Franky’s Tips:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No books found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No books found in Trash.', 'your-plugin-textdomain' )
    );

    $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'franky_tips' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'taxonomies'          => array( 'category' ),
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
    );

    register_post_type( 'franky_tips', $args );
}




/* ================== Pagination =================== */
  // numbered pagination
  function pagination($pages = '', $range = 4) {  
       $showitems = ($range * 2)+1;  
   
       global $paged;
       if(empty($paged)) $paged = 1;
   
       if($pages == '')
       {
           global $wp_query;
           $pages = $wp_query->max_num_pages;
           if(!$pages)
           {
               $pages = 1;
           }
       }   
   
       if(1 != $pages)
       {
           echo "<div class=\"pagination\"><span>Page ".$paged." of ".$pages."</span>";
           if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
           if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";
   
           for ($i=1; $i <= $pages; $i++)
           {
               if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
               {
                   echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
               }
           }
   
           if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";  
           if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
           echo "</div>\n";
       }
  }
  
  // Gravity Form remove title and use placeholder
  add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' ); 
  
  add_action('after_setup_theme', 'remove_admin_bar');

// Hide USER admin bar 
function remove_admin_bar() {
if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
}
}

/**
* Gravity Forms Custom Activation Template
* http://gravitywiz.com/customizing-gravity-forms-user-registration-activation-page
*/
add_action('wp', 'custom_maybe_activate_user', 9);
function custom_maybe_activate_user() {

    $template_path = STYLESHEETPATH . '/gfur-activate-template/activate.php';
    $is_activate_page = isset( $_GET['page'] ) && $_GET['page'] == 'gf_activation';
    
    if( ! file_exists( $template_path ) || ! $is_activate_page  )
        return;
    
    require_once( $template_path );
    
    exit();
}

function login_redirect( $redirect_to, $request, $user ){
    return home_url();
}
add_filter( 'login_redirect', 'login_redirect', 10, 3 );


// Add User list in test drive form. field ID 4
// This adds display names for all users to a drop down box on a gravity form.
add_filter("gform_pre_render", "populate_userdrop");

//Note: when changing drop down values, we also need to use the gform_admin_pre_render so that the right values are displayed when editing the entry.
add_filter("gform_admin_pre_render", "populate_userdrop");

function populate_userdrop($form){

    //only populating drop down for form id 1 - if editing this change to your own form ID
    if($form["id"] != 3)

    return $form;

    //Creating item array.
    $items = array();
    // Get the custom field values stored in the array
  // If editing this lookup where you would like to get your data from
  // this example loads through all users of the website
    $metas = get_users('role=Professional');



if (is_array($metas))
{
// in this example we just load the display_name for each user into our drop-down field
  foreach($metas as $meta) {
    if ($meta->first_name || $meta->last_name) {
      $username = $meta->first_name. ' ' .$meta->last_name;
    } else {
      $username = $meta->display_name;
    }
    $items[] = array("value" => $meta->user_email, "text" => $username);
  }
}
    //Adding items to field id 1. Replace 1 with your actual field id. You can get the field id by looking at the input name in the markup.
    foreach($form["fields"] as &$field)
        if($field["id"] == 4){
            $field["choices"] = $items;
        }

    return $form;
}

// add class to body
add_filter( 'body_class', 'custom_class' );
function custom_class( $classes ) {
  $user_id = get_current_user_id(); 
  $user_directory_type = get_user_meta( $user_id, 'directory_type', true );
  $post = get_post($user_directory_type); 
  $slug = $post->post_name;
  $classes[] = $slug;
  return $classes;
}

// Add wp_editor Parameter
add_filter( 'teeny_mce_buttons', 'my_editor_buttons', 10, 2 );
function my_editor_buttons( $buttons, $editor_id ) {
    return array( 'bold', 'italic', 'underline', 'blockquote', 'separator', 'strikethrough', 'bullist', 'numlist', 'alignleft', 'aligncenter', 'alignright', 'undo', 'redo' );
}

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


/**
 * @Account Settings
 * @return {}
 */
if ( ! function_exists( 'docdirect_user_account_settings' ) ) {
  function docdirect_user_account_settings(){ 
    global $current_user, $wp_roles,$userdata,$post;
    $user_identity  = $current_user->ID;
    $user_name  = $current_user->nickname;


    $args = array(
        'post_type' => 'preview_user_post',
        'posts_per_page'=>-1,
        'author' => $user_identity,
    );

    // the query
    $the_query = new WP_Query( $args ); ?>

    <?php if ( $the_query->have_posts() ) : ?>
      <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <?php wp_delete_post(get_the_ID()); ?>
      <?php endwhile; ?>
      <?php wp_reset_postdata(); ?>
    <?php else : ?>
      
    <?php endif;


    // Create post object
    $my_post = array(
      'post_title'    => wp_strip_all_tags( $user_name ),
      'post_content'  => $_POST['professional_statements'],
      'post_status'   => 'publish',
      'post_author'   => $user_identity,
      'post_type'     => 'preview_user_post'
    );
     
    // Insert the post into the database
    $the_post_id = wp_insert_post( $my_post );

    //Update Basics
    if( !empty( $_POST['basics'] ) ){
      foreach( $_POST['basics'] as $key => $value ){
        update_post_meta( $the_post_id, $key, esc_attr( $value ) );
      }
    }
    
    //Professional Statements
    if( !empty( $_POST['professional_statements'] ) ){
      $professional_statements  = docdirect_sanitize_wp_editor($_POST['professional_statements']);
      update_post_meta($the_post_id,'counsellor_statements',$professional_statements);
    }

    //Treatment methods
    if( !empty( $_POST['treatment_methods'] ) ){
      $treatment_methods  = docdirect_sanitize_wp_editor($_POST['treatment_methods']);
      update_post_meta($the_post_id,'treatment_methods',$treatment_methods);
    }

    //Focus Areas
    if( !empty( $_POST['focus_areas'] ) ){
      // $focus_areas = docdirect_sanitize_wp_editor($_POST['focus_areas']);
      update_post_meta($the_post_id,'focus_areas',$_POST['focus_areas']);
    }

    //Description of Practice
    if( !empty( $_POST['description_of_practice'] ) ){
      update_post_meta( $the_post_id, 'description_of_practice', $_POST['description_of_practice']);
    }
    
    //Qualifications Experience
    if( !empty( $_POST['qualifications_experience'] ) ){
      update_post_meta( $the_post_id, 'qualifications_experience', $_POST['qualifications_experience']);
    }
    
    //Fees Information
    if( !empty( $_POST['fees_information'] ) ){
      update_post_meta( $the_post_id, 'information_of_fees', $_POST['fees_information']);
    }

    //Fees Information
    if( !empty( $_POST['clinical_experience'] ) ){
      update_post_meta( $the_post_id, 'clinical_experience', $_POST['clinical_experience']);
    }
    //Update General settings
    update_post_meta( $the_post_id, 'gender', $_POST['user_gender'] );
    update_post_meta( $the_post_id, 'video_link', esc_url( $_POST['video_url'] ) );   

    //schedule
    /*if( isset( $_POST['schedules']['mon_start'] ) && !empty( $_POST['schedules']['mon_start'] ) ){
      update_post_meta( $the_post_id, 'mon_start', $_POST['schedules']['mon_start'] );   
    }*/

    //Update Basics
    if( !empty( $_POST['schedules'] ) ){
      foreach( $_POST['schedules'] as $key => $value ){
        update_post_meta( $the_post_id, $key, $value );
      }
    }

    $preview_pecialities = get_terms( array(
        'taxonomy' => 'preview_pecialities',
        'hide_empty' => false,
    ) );

    if (!empty($_POST['specialities'])) {
    foreach( $_POST['specialities'] as $specialitieskey => $specialitiesvalue ){
      foreach( $preview_pecialities as $previewkey => $previewvalue ){
        if ($specialitiesvalue == $previewvalue->slug) {
          $term_array[] = $previewvalue->term_id;
        }
      }
    }
      wp_set_post_terms( $the_post_id, $term_array, 'preview_pecialities' );
    }


    $post_url = get_permalink( $the_post_id );
  
    $json['posturl'] = $post_url;
    $json['type'] = 'success';
    $json['message']  = esc_html__('Thank you for submitting all you details, the admin will review and contact you within 48 hours.','docdirect');
    echo json_encode($json);
    die;
  }
  add_action('wp_ajax_docdirect_user_account_settings','docdirect_user_account_settings');
  add_action( 'wp_ajax_nopriv_docdirect_user_account_settings', 'docdirect_user_account_settings' );
}

// Send User Notification
if ( ! function_exists( 'approve_notification' ) ) {
  function approve_notification(){ 
    global $current_user, $wp_roles,$userdata,$post;
    $user_identity  = $current_user->ID;
    $user_name  = $current_user->nickname;
    $full_name = docdirect_get_username($user_identity);

    // Send Mail for approvel
    $email = get_option('admin_email');   
    $to = $email;
    $subject = 'User Information Update';
    $body = $full_name. ' updated profile please review profile <a href="'.admin_url( 'user-edit.php?user_id=' . $user_identity, 'http' ).'" target="_blank">here</a>';
    $headers = array('Content-Type: text/html; charset=UTF-8');
     
    wp_mail( $to, $subject, $body, $headers );

    $json['type'] = 'success';
    $json['message']  = esc_html__('Thank you for submitting all you details, the admin will review and contact you within 48 hours.','docdirect');
    echo json_encode($json);
    die;
  }
  add_action('wp_ajax_approve_notification','approve_notification');
  add_action( 'wp_ajax_nopriv_approve_notification', 'approve_notification' );
}