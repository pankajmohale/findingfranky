<?php
/* 
	Template Name: What they do Template
*/
get_header();
?>
<div class="container">
	<div class="custom-problem-dropdown clearfix">
		<div class="row">
			<div class="col-md-4 col-sm-6 col-xs-12 col-sm-push-3 col-md-push-4 aside sidebar-section" id="sidebar">
				<div class="custom-problem-dropdown-dropdown clearfix">
					<div class="what-do-they-do-list custom-post-dropdown">
						<!-- <h4 class="panel-title">What Do They Do?</h4> -->
						<select name="problem_id" id="mainselection" onchange="location = this.value;">
						<option value="">In this section...</option>
							<?php
								global $post;
								$args = array( 
									'numberposts' => -1,
									'orderby'          => 'title',
									'order'            => 'ASC',
									'post_type'        => 'theydo',
								);
								$posts = get_posts($args);
								foreach( $posts as $post ) : setup_postdata($post);
									$permalink = get_permalink($post->ID);
								 ?>
								<option value="<?php echo $permalink; ?>"><?php the_title(); ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
			</div>		
		</div>
	</div>
	<div class="row">
		<div class="theydo">
			<div class="tips-holder">
				<?php
					$args = array(
					    'post_type' => 'theydo',
					    'post_status' => 'publish',
					    'posts_per_page' => -1,
					    'orderby' => 'title',
						'order'   => 'ASC',
					);
					$posts = new WP_Query( $args );
				
				while ( $posts->have_posts() ) : $posts->the_post();  ?>
					
					<div class="col-xs-12">
						<div class="prob_list">
							<div class="prob-inner-wrapper">
								<a href="<?php the_permalink(); ?>">
									<h1><?php the_title(); ?></h1>
								</a>
								<?php the_excerpt(); ?>
								<a href="<?php echo get_the_permalink(); ?>" class="btn custom-btn"><?php echo 'Read More'; ?></a>
							</div>
						</div>
					</div>
					
				<?php endwhile; ?>				
			</div>		
		</div>
	</div>
</div>
<?php
get_footer();
?>
