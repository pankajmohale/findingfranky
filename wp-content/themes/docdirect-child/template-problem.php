<?php
/* 
	Template Name: Problem Template
*/
get_header();
?>
<div class="container">
	<div class="custom-problem-dropdown clearfix">
		<div class="row">
			<div class="col-sm-6 col-md-4 col-xs-12 col-sm-push-3 col-md-push-4 aside sidebar-section" id="sidebar">
				<div class="what-is-the-problem-list custom-post-dropdown">
					<!-- <h4 class="panel-title">What is the Problem?</h4> -->
					<select name="problem_id" id="mainselection" onchange="location = this.value;">
					<option value="">In this section...</option>
						<?php
							global $post;
							$args = array( 
								'numberposts' => -1,
								'orderby'          => 'title',
								'order'            => 'ASC',
								'post_type'        => 'problems',
							);
							$posts = get_posts($args);
							foreach( $posts as $post ) : setup_postdata($post);
								$permalink = get_permalink($post->ID);
							 ?>
							<option value="<?php echo $permalink; ?>"><?php the_title(); ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="what-is-the-problem">
			<div class="col-xs-12">
				<div class="row">
					<div class="what-is-the-problem">
						<?php
							$args = array(
							    'post_type' => 'problems',
							    'post_status' => 'publish',
							    'posts_per_page' => -1,
							    'orderby' => 'title',
								'order'   => 'ASC',
							);
							$posts = new WP_Query( $args );
						
							while ( $posts->have_posts() ) : $posts->the_post();  ?>
							
								<div class="col-xs-12">
									<div class="prob_list">
										<div class="prob-inner-wrapper">
											<a href="<?php the_permalink(); ?>">
												<h1><?php the_title(); ?></h1>
											</a>
											<?php the_excerpt(); ?>
											<a href="<?php echo get_the_permalink(); ?>" class="btn custom-btn"><?php echo 'Read More'; ?></a>
										</div>
									</div>
								</div>
							
						<?php  endwhile; ?>
					</div>					
				</div>
			</div>
		</div>
<?php
get_footer();
?>
