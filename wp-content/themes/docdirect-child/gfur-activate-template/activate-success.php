<?php

global $gw_activate_template;

extract( $gw_activate_template->result );

$url = is_multisite() ? get_blogaddress_by_id( (int) $blog_id ) : home_url('', 'http');
$user = new WP_User( (int) $user_id );

?>

<div class="container">

	<div class="row">
		<div class="col-xs-12">
			<h2><?php _e('Your account is now active!'); ?></h2>

			<div id="signup-welcome">
			    <p><span class="h3"><?php _e('Username:'); ?></span> <?php echo $user->user_login ?></p>
			    <p><span class="h3"><?php _e('Password:'); ?></span> <?php echo $password; ?></p>
			</div>			
		</div>
	</div>
</div>