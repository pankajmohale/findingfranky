<?php
/* 
	Template Name: Franky Tips Template
*/
get_header();
?>
	<div class="container">
		<div class="row">
			<div class="col-sm-push-4 col-sm-4">
				<div class="add-update-tips">
						<a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Add Tips' ) ) ); ?>" class="btn custom-btn">Add Tips</a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-sm-4 col-xs-12 aside sidebar-section" id="sidebar">
				<div class="aside sidebar-section" id="sidebar">
					<aside id="tg-sidebar" class="tg-sidebar tg-haslayout ">
						<?php //get_sidebar();?>
							<?php
							$tipsargs = array(
							    'post_type' => 'franky_tips',
							    'post_status' => 'publish',
							    'posts_per_page' => -1,
							    'paged' => $paged,
							);
							$tipsquery = new WP_Query( $tipsargs ); ?>

					    	<?php 
							if ( wp_is_mobile() ) { ?>
								<?php //dynamic_sidebar( 'tips-mobile' ); ?>
								<?php
									$poctcount = 1; ?>
										<div class="tg-widget tips-normal">
											<h3>Recent Tips</h3>
											<?php if ( $tipsquery->have_posts() ) : ?>

													<!-- pagination here -->
													<ul >
														<!-- the loop -->
														<?php while ( $tipsquery->have_posts() ) : $tipsquery->the_post(); ?>
																<li>
																	<a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a>
																	<span class="post-date"><?php echo get_the_date('F j, Y'); ?></span>
																</li>
																
																
															<?php if ( $poctcount == 6 ) { ?>
																</ul><ul class="collapse" id="collapseExample">
															<?php } ?>
															<?php $poctcount++; ?>
														<?php endwhile; ?>
														<!-- end of the loop -->
													</ul>

													<!-- pagination here -->

													<?php wp_reset_postdata(); ?>

												<?php else : ?>
													<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
												<?php endif; ?>
										</div>
										<?php
								if ($tipsquery->post_count > 6) { ?> 
									<button class="btn btn-primary expand-tips-btn" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
									<i class="fa fa-plus" aria-hidden="true"></i>
									</button>
								<?php } ?>


							<?php } else { ?>
							
								<?php //dynamic_sidebar( 'tips-desktop' ); ?>
								<?php
								if ($tipsquery->post_count <= 6) { ?>
									
									<div class="tg-widget tips-normal">
										<h3>Recent Tips</h3>
										<?php if ( $tipsquery->have_posts() ) : ?>

												<!-- pagination here -->
												<ul >
													<!-- the loop -->
													<?php while ( $tipsquery->have_posts() ) : $tipsquery->the_post(); ?>
														<li>
															<a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a>
															<span class="post-date"><?php echo get_the_date('F j, Y'); ?></span>
														</li>
															
													<?php endwhile; ?>
													<!-- end of the loop -->
												</ul>

												<!-- pagination here -->

												<?php wp_reset_postdata(); ?>

											<?php else : ?>
												<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
											<?php endif; ?>
									</div>


								<?php } else { ?>

									<div class="tg-widget tips-desktop">
										<h3>Recent Tips</h3>
										<?php if ( $tipsquery->have_posts() ) : ?>

											<!-- pagination here -->
											<ul>
												<!-- the loop -->
												<?php while ( $tipsquery->have_posts() ) : $tipsquery->the_post(); ?>
													<li>
														<a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a>
														<span class="post-date"><?php echo get_the_date('F j, Y'); ?></span>
													</li>
														
												<?php endwhile; ?>
												<!-- end of the loop -->
											</ul>

											<!-- pagination here -->

											<?php wp_reset_postdata(); ?>

										<?php else : ?>
											<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
										<?php endif; ?>
									</div>
								<?php } ?>
							<?php }	?>							

					</aside>
				</div>
			</div>
			<div class="col-lg-9 col-sm-8 col-xs-12">
				<div class="tips-holder">
				
					<?php
					$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
					$args = array(
					    'post_type' => 'franky_tips',
					    'post_status' => 'publish',
					    'posts_per_page' => 10,
					    'paged' => $paged,
					);
					$tipsposts = new WP_Query( $args );?>
				
					<?php if ( $tipsposts->have_posts() ) : ?>

			        <div class="franky_tips_wrapper">
					<!-- pagination here -->
					    <!-- the loop -->
					    <?php while ( $tipsposts->have_posts() ) : $tipsposts->the_post(); ?>
				            	<div class="franky_tips_container">
						            <h2><a href="<?php echo esc_url( get_the_permalink() ); ?>"><?php the_title(); ?> </a></h2>
						            <?php the_excerpt(); ?><a href="<?php the_permalink(); ?>" class="franky_tips_links">Read more...</a>
				            		<!-- <a href="<?php echo site_url().'/update-tips/?post_id='.get_the_ID(); ?>" class="franky_tips_links">Edit Post</a> -->
				            	</div>

					    <?php endwhile; ?>
					    <!-- end of the loop -->

					<!-- pagination here -->
			        </div>

					<?php if (function_exists("pagination")) {
					    pagination($tipsposts->max_num_pages);
					} ?>

					<?php wp_reset_postdata(); ?>

					<?php else : ?>
					<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
					<?php endif; ?>
					
				</div>	
			</div>			
		</div>
	</div>
<?php
get_footer();
?>
