<?php
/**
 * The template for displaying user detail
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Doctor Directory
 */
global $wp_query,$current_user;
$current_author_profile = $wp_query->get_queried_object();
do_action('docdirect_update_profile_hits',$current_author_profile->ID); //Update Profile Hits
docdirect_set_user_views($current_author_profile->ID); //Update profile views
get_header();//Include Headers

if ( apply_filters( 'docdirect_is_visitor', $current_author_profile->ID ) === false ) {	
$avatar = apply_filters(
				'docdirect_get_user_avatar_filter',
				 docdirect_get_user_avatar(array('width'=>365,'height'=>365), $current_author_profile->ID),
				 array('width'=>365,'height'=>365) //size width,height
			);


$banner	= docdirect_get_user_banner(array('width'=>1920,'height'=>450), $current_author_profile->ID);

$current_date 	  = date('Y-m-d H:i:s');
$current_string	= strtotime( $current_date );
$featured_string   = $current_author_profile->user_featured;
$user_gallery	  = $current_author_profile->user_gallery;
$directory_type	= $current_author_profile->directory_type;
$contact_form	  = $current_author_profile->contact_form;
$uni_flag 		  = rand(1,9999);
$enable_login      = '';
$user_profile_specialities = '';
$education_switch  = '';

$facebook	  = isset( $current_author_profile->facebook ) ? $current_author_profile->facebook : '';
$twitter	   = isset( $current_author_profile->twitter ) ? $current_author_profile->twitter : '';
$linkedin	  = isset( $current_author_profile->linkedin ) ? $current_author_profile->linkedin : '';
$pinterest	 = isset( $current_author_profile->pinterest ) ? $current_author_profile->pinterest : '';
$google_plus   = isset( $current_author_profile->google_plus ) ? $current_author_profile->google_plus : '';
$instagram	 = isset( $current_author_profile->instagram ) ? $current_author_profile->instagram : '';
$tumblr	    = isset( $current_author_profile->tumblr ) ? $current_author_profile->tumblr : '';
$skype	  	 = isset( $current_author_profile->skype ) ? $current_author_profile->skype : '';
$professional_statements	  	 = isset( $current_author_profile->professional_statements ) ? $current_author_profile->professional_statements : '';
$treatment_methods	  	 = isset( $current_author_profile->treatment_methods ) ? $current_author_profile->treatment_methods : '';
$focus_areas	  	 = isset( $current_author_profile->focus_areas ) ? $current_author_profile->focus_areas : '';
$description_of_practice	  	 = isset( $current_author_profile->description_of_practice ) ? $current_author_profile->description_of_practice : '';
$qualifications_experience	  	 = isset( $current_author_profile->description_of_practice ) ? $current_author_profile->qualifications_experience : '';
$fees_information	  	 = isset( $current_author_profile->fees_information ) ? $current_author_profile->fees_information : '';

$schedule_time_format  = isset( $current_author_profile->time_format ) ? $current_author_profile->time_format : '12hour';

if(function_exists('fw_get_db_settings_option')) {
	$enable_login = fw_get_db_settings_option('enable_login', $default_value = null);
	$dir_map_marker    = fw_get_db_post_option($directory_type, 'dir_map_marker', true);
	$dir_map_marker_default = fw_get_db_settings_option('dir_map_marker');
	$education_switch    = fw_get_db_post_option($directory_type, 'education', true);
	$claims_switch    = fw_get_db_post_option($directory_type, 'claims', true);
	$experience_switch    = fw_get_db_post_option($directory_type, 'experience', true);
	$price_switch    = fw_get_db_post_option($directory_type, 'price_list', true);
	$reviews_switch    = fw_get_db_post_option($directory_type, 'reviews', true);
	$awards_switch    = fw_get_db_post_option($directory_type, 'awards', true);
	$user_profile_specialities    = fw_get_db_post_option($directory_type, 'user_profile_specialities', true);
	$theme_type = fw_get_db_settings_option('theme_type');
	$theme_color = fw_get_db_settings_option('theme_color');
}

if( !empty( $dir_map_marker['url'] ) ){
	$dir_map_marker  = $dir_map_marker['url'];
} else{
	if( !empty( $dir_map_marker_default['url'] ) ){
		$dir_map_marker	 = $dir_map_marker_default['url'];
	} else{
		$dir_map_marker	 	   = get_template_directory_uri().'/images/map-marker.png';
	}
}

$privacy		= docdirect_get_privacy_settings($current_author_profile->ID); //Privacy settings

docdirect_enque_map_library();//init Map
docdirect_enque_rating_library();//rating
wp_enqueue_script('intlTelInput');
wp_enqueue_style('intlTelInput');

$apointmentClass	= 'appointment-disabled';
if( !empty( $privacy['appointments'] )
	&& 
	$privacy['appointments'] == 'on'
 ) {
	$apointmentClass	= 'appointment-enabled';
	if( function_exists('docdirect_init_stripe_script') ) {
		//Strip Init
		docdirect_init_stripe_script();
	}
	
	if( isset( $current_user->ID ) 
	 && 
		$current_user->ID != $current_author_profile->ID
	){
		$apointmentClass	= 'appointment-enabled';
	} else{
		$apointmentClass	= 'appointment-disabled';
	}
}

$review_data	= docdirect_get_everage_rating ( $current_author_profile->ID );
docdirect_init_dir_map();//init Map
docdirect_enque_map_library();//init Map
$banner_parallax	= '';
if( !empty( $banner ) ){
	$banner_parallax	= 'data-appear-top-offset="600" data-parallax="scroll" data-image-src="'.$banner.'"';
}

//rating star color
if ( isset( $theme_type) && $theme_type === 'custom') {
	if ( !empty( $theme_color ) ) {
		$rating_color	= $theme_color;
	} else{
		$rating_color	= '#7dbb00';
	}
} else {
	$rating_color	= '#7dbb00';
}

?>
<div class="container">
  <div class="row">
    <div class="tg-userdetail custom-user_info <?php echo sanitize_html_class( $apointmentClass );?>">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <button class="back-button btn" onclick="goBack()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
		<script>
			function goBack() {
			    window.history.back();
			}
		</script>
        <aside id="tg-sidebar" class="tg-sidebar">
          <div class="tg-widget tg-widgetuserdetail">
            <figure class="tg-userimg"> 
            	<a href="<?php echo esc_url(get_author_posts_url($current_author_profile->ID));?>"><img src="<?php echo esc_attr( $avatar );?>" alt="<?php echo esc_attr( $current_author_profile->first_name.' '.$current_author_profile->last_name );?>"></a>
              <figcaption>
                <ul class="tg-featureverified">
				  <?php if( isset( $featured_string ) && $featured_string > $current_string ){?>
                        <li class="tg-featuresicon"><a href="javascript:;"><i class="fa fa-bolt"></i><span><?php esc_html_e('featured','docdirect');?></span></a></li>
                  <?php }?>
                  <?php docdirect_get_verified_tag(true,$current_author_profile->ID,'simple');?>
                </ul>
              </figcaption>
            </figure>
          </div>
        </aside>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="tg-haslayout custom-user-content">
          <div class="tg-userbanner-content">
                <h1><?php echo esc_attr( $current_author_profile->first_name.' '.$current_author_profile->last_name );?></h1>
                <?php if( !empty( $current_author_profile->tagline ) ) {?>
                <h3><?php echo esc_attr( $current_author_profile->tagline );?></h3>
                <?php }?>
          </div>
          <div class="tg-aboutuser">
            <?php if( !empty( $current_author_profile->description ) ) {?>
              <div class="tg-description">
                <p><?php echo wpautop( $current_author_profile->description );?></p>
              </div>
            <?php }?>
          </div>
          <?php if( !empty( $current_author_profile->latitude ) && !empty( $current_author_profile->longitude ) ) {?>
          <div class="tg-section-map">
          <div id="map_canvas" class="tg-location-map tg-haslayout"></div>
          <?php do_action('docdirect_map_controls');?>
          <?php
		  	$directories	= array();
			$directories_array	= array();
		   	$directories['status']	= 'found';
			$directories_array['latitude']	= $current_author_profile->latitude;
			$directories_array['longitude']	= $current_author_profile->longitude;
			$directories_array['title']	= $current_author_profile->display_name;
			$directories_array['name']	 = $current_author_profile->first_name.' '.$current_author_profile->last_name;
			$directories_array['email']	 		 = $current_author_profile->user_email;
			$directories_array['phone_number']	 = $current_author_profile->phone_number;
			$directories_array['address']	 = $current_author_profile->user_address;
			$directories_array['group']	= '';
			$directories_array['icon']	 	   = $dir_map_marker;
			$avatar = apply_filters(
										'docdirect_get_user_avatar_filter',
										 docdirect_get_user_avatar(array('width'=>150,'height'=>150), $current_author_profile->ID),
										 array('width'=>150,'height'=>150) //size width,height
									);
			
			$infoBox	= '<div class="tg-mapmarker">';
			$infoBox	.= '<figure><img width="60" heigt="60" src="'.esc_url( $avatar ).'" alt="'.esc_attr__('User','docdirect').'"></figure>';
			$infoBox	.= '<div class="tg-mapmarkercontent">';
			$infoBox	.= '<h3><a href="'.get_author_posts_url($current_author_profile->ID).'">'.$directories_array['name'].'</a></h3>';
			
			if( !empty( $current_author_profile->tagline ) ) {
				$infoBox	.= '<span>'.$current_author_profile->tagline.'</span>';
			}
			
			
			/* $infoBox	.= '<ul class="tg-likestars">';
			
			if( isset( $reviews_switch ) && $reviews_switch === 'enable' && !empty( $review_data )){
				$infoBox	.= '<li>'.docdirect_get_rating_stars($review_data,'return','hide').'</li>';
			}
			$infoBox	.= '<li>'.docdirect_get_wishlist_button($current_author_profile->ID,false).'</li>';
			$infoBox	.= '<li>'.docdirect_get_user_views($current_author_profile->ID).'&nbsp;'.esc_html__('view(s)','docdirect').'</li>';
			
			$infoBox	.= '</ul>'; */
			$infoBox	.= '</div>';
																
			$directories_array['html']['content']	= $infoBox;
			$directories['users_list'][]	= $directories_array;
		   ?>
           <script>
				jQuery(document).ready(function() {
					docdirect_init_detail_map_script(<?php echo json_encode( $directories );?>);
				});	
		  </script>
          </div> 
          <?php }?>
			<?php if( !empty( $professional_statements ) ){?>
				<div class="user-more-description">
					<div class="professional-statements">
				    	<?php echo do_shortcode( nl2br( $professional_statements));?>
				    </div>
				</div>
			<?php }?>          		

			<!--Video URL-->
          <?php if( isset( $current_author_profile->video_url ) && !empty( $current_author_profile->video_url ) ) {?>
	          <div class="tg-presentationvideo">
	            <?php
					$height = 200;
					$width  = 368;
					$post_video = $current_author_profile->video_url;
					$url = parse_url( $post_video );
					if ($url['host'] == $_SERVER["SERVER_NAME"]) {
						echo '<div class="video">';
						echo do_shortcode('[video width="' . $width . '" height="' . $height . '" src="' . $post_video . '"][/video]');
						echo '</div>';
					} else {

						if ($url['host'] == 'vimeo.com' || $url['host'] == 'player.vimeo.com') {
							echo '<div class="video">';
							$content_exp = explode("/", $post_video);
							$content_vimo = array_pop($content_exp);
							echo '<iframe width="' . $width . '" height="' . $height . '" src="https://player.vimeo.com/video/' . $content_vimo . '" 
	></iframe>';
							echo '</div>';
						} elseif ($url['host'] == 'soundcloud.com') {
							$video = wp_oembed_get($post_video, array('height' => $height));
							$search = array('webkitallowfullscreen', 'mozallowfullscreen', 'frameborder="no"', 'scrolling="no"');
							echo '<div class="audio">';
							$video = str_replace($search, '', $video);
							echo str_replace('&', '&amp;', $video);
							echo '</div>';
						} else {
							echo '<div class="video">';
							$content = str_replace(array('watch?v=', 'http://www.dailymotion.com/'), array('embed/', '//www.dailymotion.com/embed/'), $post_video);
							echo '<iframe width="' . $width . '" height="' . $height . '" src="' . $content . '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
							echo '</div>';
						}
					}
				?>
	          </div>
	          <?php }?>

			<?php if( !empty( $treatment_methods ) ){?>
				<div class="tg-treatment-methods">
					<div class="tg-userheading">
						<h2><?php esc_html_e('Treatment Methods','docdirect');?></h2>
					</div>
					<div class="professional-statements">
						<?php echo do_shortcode( nl2br( $treatment_methods));?>
					</div>
				</div>
			<?php }?>
          
          <?php if (is_active_sidebar('user-page-top')) {?>
              <div class="tg-doctors-list tg-haslayout user-ad-top">
                <?php dynamic_sidebar('user-page-top'); ?>
              </div>
          <?php }?>
			<?php if( !empty( $focus_areas ) || !empty( $current_author_profile->user_profile_specialities ) ){?>
				<div class="user-qualities">
					<div class="row">
						<?php if( !empty( $focus_areas ) ){?>
							<div class="col-xs-12 col-sm-6">
								<div class="tg-focus-areas">
									<div class="tg-userheading">
										<h2><?php esc_html_e('Focus Areas','docdirect');?></h2>
									</div>
									<div class="focus-areas">
										<?php echo do_shortcode( $focus_areas);?>
									</div>
								</div>
							</div>
						<?php }?>
						<?php if( !empty( $current_author_profile->user_profile_specialities ) ) {?>
							<div class="col-xs-12 col-sm-6">
							  <!--Specialities-->
							      <div class="tg-honourawards tg-listview-v3 user-section-style sp-icon-wrap">
							        <div class="tg-userheading">
							          <h2><?php esc_html_e('Additional Specialities / Services','docdirect');?></h2>
							        </div>
							        <div class="tg-doctor-profile">
							              <ul class="tg-tags">
							                  <?php
												do_action('enqueue_unyson_icon_css');														
							                    foreach( $current_author_profile->user_profile_specialities as $key => $value ){
													$get_speciality_term = get_term_by('slug', $key, 'specialities');
													$speciality_title = '';
													$term_id = '';
													if (!empty($get_speciality_term)) {
														$speciality_title = $get_speciality_term->name;
														$term_id = $get_speciality_term->term_id;
													}

													$speciality_meta = array();
													if (function_exists('fw_get_db_term_option')) {
														$speciality_meta = fw_get_db_term_option($term_id, 'specialities');
													}
													
													$speciality_icon = array();
													if (!empty($speciality_meta['icon']['icon-class'])) {
														$speciality_icon = $speciality_meta['icon']['icon-class'];
													}
							                     ?>
							                    <li class="specialities-wrap">
							                    	
						                    		<!-- <span class="icon-sp">
						                    			<?php 
														if ( isset($speciality_meta['icon']['type']) && $speciality_meta['icon']['type'] === 'icon-font') {
															if (!empty($speciality_icon)) { ?>
																<i class="<?php echo esc_attr($speciality_icon); ?>"></i>
															<?php 
															}
														} else if ( isset($speciality_meta['icon']['type']) && $speciality_meta['icon']['type'] === 'custom-upload') {
															if (!empty($speciality_meta['icon']['url'])) {
															?>
															<img src="<?php echo esc_url($speciality_meta['icon']['url']);?>">
														<?php }}?>
						                    		</span> -->                   		
						                   			<span><?php echo esc_attr( $value );?></span>
							                    	
							                    </li>
							                  <?php }?>
							              </ul>
							          </div>
							      </div>
							</div>
							  <?php }?>
					</div>          	
				</div>
			<?php }?>
          
			<?php if( !empty( $description_of_practice ) ){?>
          		<div class="tg-description_of_practice">
					<div class="tg-userheading">
						<h2><?php esc_html_e('Description of Practice','docdirect');?></h2>
					</div>
						<div class="professional-statements">
							<?php echo do_shortcode( nl2br( $description_of_practice));?>
						</div>
				</div>
			<?php }?>
			<?php if( !empty( $qualifications_experience ) || !empty( $fees_information ) ){?>
	          	<div class="qualification-info">
		          	<div class="row">
				        <?php if( !empty( $qualifications_experience ) ){?>
		          			<div class="col-xs-12 col-sm-6">
				          		<div class="tg-qualifications_experience">
									<div class="tg-userheading">
										<h2><?php esc_html_e('Qualifications / Experience','docdirect');?></h2>
									</div>
										<div class="professional-statements">
											<?php echo do_shortcode( $qualifications_experience);?>
										</div>
								</div>
		          			</div>
						<?php }?>
	          			<?php if( !empty( $fees_information ) ){?>
			          		<div class="col-xs-12 col-sm-6">
				          		<div class="tg-fees_information">
									<div class="tg-userheading">
										<h2><?php esc_html_e('Information on fees','docdirect');?></h2>
									</div>
									<div class="professional-statements">
										<?php echo do_shortcode( $fees_information);?>
									</div>
								</div>
			          		</div>
						<?php }?>
		          	</div>
		        </div>
			<?php }?>
            
            <div class="counsellor_info">
            	<div class="row">
            		<div class="col-sm-7">
            			<div class="tg-userschedule user_info">
	            			<div class="tg-userheading">
	            				<h2><?php esc_html_e('Contact','docdirect');?></h2>
	            			</div>
		        			<ul class="tg-doccontactinfo">
				                <?php if( !empty( $current_author_profile->user_address ) ) {?>
				                	<li> <i class="fa fa-map-marker"></i> <address><?php echo esc_attr( $current_author_profile->user_address );?></address> </li>
				                <?php }?>
				                <?php if( !empty( $current_author_profile->user_email ) 
										  &&
										  !empty( $privacy['email'] )
										  && 
										  $privacy['email'] == 'on'
								) {?>
				                    <li><i class="fa fa-envelope-o"></i><a href="mailto:<?php echo esc_attr( $current_author_profile->user_email );?>?subject:<?php esc_html_e('Hello','docdirect');?>"><?php echo esc_attr( $current_author_profile->user_email );?></a></li>
				                <?php }?>
				                <?php if( !empty( $current_author_profile->phone_number ) 
										  &&
										  !empty( $privacy['phone'] )
										  && 
										  $privacy['phone'] == 'on'
								) {?>
				                	<li> <i class="fa fa-phone"></i> <span><?php echo esc_attr( $current_author_profile->phone_number );?></span> </li>
				                <?php }?>
				                <?php if( !empty( $current_author_profile->fax ) ) {?>
				                	<li><i class="fa fa-fax"></i> <span><?php echo esc_attr( $current_author_profile->fax );?></span> </li>
				                <?php }?>
				                <?php if( !empty( $current_author_profile->skype ) ) {?> 
				                	<li><i class="fa fa-skype"></i><span><?php echo esc_attr( $current_author_profile->skype );?></span></li>
				                <?php }?>
				                <?php if( !empty( $current_author_profile->user_url ) ) {?>
				                    <li><i class="fa fa-link"></i><a href="<?php echo esc_url( $current_author_profile->user_url );?>" target="_blank"><?php echo docdirect_parse_url( $current_author_profile->user_url);?></a></li>
				                <?php }?>
				              </ul>
				              <a href="<?php echo get_permalink( get_page_by_path( 'test-drive' ) ); ?>?counsellor=<?php echo esc_attr( $current_author_profile->user_email );?>" class="btn custom-btn">Free Test Drive</a>
            			</div>
            		</div>
            		<div class="col-sm-5">
            			<?php
						if( apply_filters('docdirect_is_setting_enabled',$current_author_profile->ID,'schedules' ) === true ){
							/*if( !empty( $privacy['opening_hours'] )
								  && 
								  $privacy['opening_hours'] == 'on'
							 ) { */?>
							<div class="tg-userschedule">
								<div class="tg-userheading"><h2><?php esc_html_e('Practice Hours','docdirect');?></h2></div>
							<ul>
							<?php 
								$week_array	= docdirect_get_week_array();
								$db_schedules	= array();
								if( isset( $current_author_profile->schedules ) && !empty( $current_author_profile->schedules ) ){
									$db_schedules	= $current_author_profile->schedules;
								}

								//Time format
								if( isset( $schedule_time_format ) && $schedule_time_format === '24hour' ){
									$time_format	= 'G:i';
								} else{
									$time_format	= 'g:i A';
								}

								$date_prefix	= date('D');
								if( isset( $week_array ) && !empty( $week_array ) ) {
								foreach( $week_array as $key => $value ){
									$start_time_formate	 = '';
									$end_time_formate	   = '';
									$start_time  = $db_schedules[$key.'_start'];
									$end_time	= $db_schedules[$key.'_end'];

									if( !empty( $start_time ) ){
										$start_time_formate	= date( $time_format, strtotime( $start_time ) );
									}


									if( isset( $end_time ) && !empty( $end_time ) ){ 
										$end_time_formate	= date( $time_format, strtotime( $end_time ) );
										$end_time_formate	= docdirect_date_24midnight($time_format,strtotime( $end_time ));
									}

									//Active day
									$active	= '';
									if( strtolower( $date_prefix ) == $key ){
										$active	= 'current';
									}

									//
									if( !empty( $start_time_formate ) && $end_time_formate ) {
										$data_key	= $start_time_formate.' - '.$end_time_formate;
									} else if( !empty( $start_time_formate ) ){
										$data_key	= $start_time_formate;
									} else if( !empty( $end_time_formate ) ){
										$data_key	= $end_time_formate;
									} else{
										$data_key	= esc_html__('Closed','docdirect');
									}
								?>
								<li class="<?php echo sanitize_html_class( $active );?>"><a href="javascript:;" data-type="<?php echo esc_attr( $data_key );?>"><span><?php echo esc_attr( $value );?></span><em><?php echo esc_attr( $data_key );?></em></a></li>

							<?php /*}*/}?>
						</ul>
						</div>
		             	<?php }}?>
            		</div>
            	</div>
            </div>  
                    
          <!--Reviews-->
          <!-- <?php if( isset( $reviews_switch ) && $reviews_switch === 'enable' ){?>
              <div class="tg-userreviews">
                <div class="tg-userheading">
                  <h2><?php echo intval( apply_filters('docdirect_count_reviews',$current_author_profile->ID) );?>&nbsp;&nbsp;<?php esc_html_e('Review(s)','docdirect');?></h2> 
                </div>
                <?php if( !empty( $review_data['by_ratings'] ) ) {?>
                <div class="tg-ratingbox">
                  <div class="tg-averagerating">
                    <h3><?php esc_html_e('Average Rating','docdirect');?></h3>
                    <em><?php echo number_format((float)$review_data['average_rating'], 1, '.', '');?></em>
                    <span class="tg-stars"><?php docdirect_get_rating_stars($review_data,'echo','hide');?></span>
                  </div>
                  <div id="tg-userskill" class="tg-userskill">
                    <?php 
                        foreach( $review_data['by_ratings'] as $key => $value ){
                            $final_rate = 0;
                            if( !empty( $value['rating'] ) && !empty( $value['rating'] ) ) {
                                $get_sum	  = $value['rating'];
                                $get_total	= $value['total'];
                                $final_rate	= $get_sum/$get_total*100;
                            } else{
                                $final_rate	= 0;
                            }
                            
                        ?>
                        <div class="tg-skill"> 
                          <span class="tg-skillname"><?php echo intval( $key+1 );?> <?php esc_html_e('Stars','docdirect');?></span> 
                          <span class="tg-skillpercentage"><?php echo intval($final_rate/5);?>%</span>
                          <div class="tg-skillbox">
                            <div class="tg-skillholder" data-percent="<?php echo intval($final_rate/5);?>%">
                              <div class="tg-skillbar"></div>
                            </div>
                          </div>
                        </div>
                    <?php }?>
                  </div>
                </div>
                <?php }?>
                <ul class="tg-reviewlisting">
                <?php if( apply_filters('docdirect_count_reviews',$current_author_profile->ID) > 0 ){
                global $paged;
                if (empty($paged)) $paged = 1;
                $show_posts    = get_option('posts_per_page') ? get_option('posts_per_page') : '-1';        
                
                $meta_query_args = array('relation' => 'AND',);
                $meta_query_args[] = array(
                                        'key' 	   => 'user_to',
                                        'value' 	 => $current_author_profile->ID,
                                        'compare'   => '=',
                                        'type'	  => 'NUMERIC'
                                    );
                
                $args = array('posts_per_page' => "-1", 
                    'post_type' => 'docdirectreviews', 
                    'order' => 'DESC', 
                    'orderby' => 'ID', 
                    'post_status' => 'publish', 
                    'ignore_sticky_posts' => 1
                );
                
                $args['meta_query'] = $meta_query_args;
                
                $query 		= new WP_Query( $args );
                $count_post = $query->post_count;        
                
                //Main Query	
                $args 		= array('posts_per_page' => $show_posts, 
                    'post_type' => 'docdirectreviews', 
                    'paged' => $paged, 
                    'order' => 'DESC', 
                    'orderby' => 'ID', 
                    'post_status' => 'publish', 
                    'ignore_sticky_posts' => 1
                );
                
                $args['meta_query'] = $meta_query_args;
                
                $query 		= new WP_Query($args);
                if( $query->have_posts() ){
                    while($query->have_posts()) : $query->the_post();
                        global $post;
                        $user_rating = fw_get_db_post_option($post->ID, 'user_rating', true);
                        $user_from = fw_get_db_post_option($post->ID, 'user_from', true);
                        $review_date  = fw_get_db_post_option($post->ID, 'review_date', true);
                        $user_data 	  = get_user_by( 'id', intval( $user_from ) );
                        
                        $avatar = apply_filters(
                                        'docdirect_get_user_avatar_filter',
                                         docdirect_get_user_avatar(array('width'=>150,'height'=>150), $user_from),
                                         array('width'=>150,'height'=>150) //size width,height
                                    );
                        
                        $user_name	= '';
                        if( !empty( $user_data ) ) {
                            $user_name	= $user_data->first_name.' '.$user_data->last_name;
                        }
                        
                        if( empty( $user_name ) && !empty( $user_data ) ){
                            $user_name	= $user_data->user_login;
                        }
                        
                        $percentage	= $user_rating*20;
                        
                    ?>
                    <li>
                        <div class="tg-review">
                          <figure class="tg-reviewimg"> 
                            <a href="<?php echo get_author_posts_url($user_from); ?>"><img src="<?php echo esc_url( $avatar );?>" alt="<?php esc_html_e('Reviewer','docdirect');?>"></a>
                          </figure>
                          <div class="tg-reviewcontet">
                            <div class="tg-reviewhead">
                              <div class="tg-reviewheadleft">
                                <h3><a href="<?php echo get_author_posts_url($user_from); ?>"><?php echo esc_attr( $user_name );?></a></h3>
                                <span><?php echo human_time_diff( strtotime( $review_date ), current_time('timestamp') ) . ' ago'; ?></span> </div>
                              <div class="tg-reviewheadright tg-stars star-rating">
                                <span style="width:<?php echo esc_attr( $percentage );?>%"></span>
                              </div>
                            </div>
                            <div class="tg-description">
                              <p><?php the_content();?></p>
                            </div>
                          </div>
                        </div>
                      </li>
                    <?php 
                        endwhile; wp_reset_postdata();
                    }else{?>
                        <li class="noreviews-found"> <?php DoctorDirectory_NotificationsHelper::informations(esc_html__('No Reviews Found.','docdirect'));;?></li>
                    <?php }
                } else{?>
                    <li class="noreviews-found"> <?php DoctorDirectory_NotificationsHelper::informations(esc_html__('No Reviews Found.','docdirect'));;?></li>
                <?php }?>
                  
                </ul>
                <?php 
                if( isset( $current_user->ID ) 
                    && 
                    $current_user->ID != $current_author_profile->ID 
                ){?>
                <div class="tg-leaveyourreview">
                  <div class="tg-userheading">
                    <h2><?php esc_html_e('Leave Your Review','docdirect');?></h2>
                  </div>
                  <?php if( apply_filters('docdirect_is_user_logged_in','check_user') === true
                        && $enable_login === 'enable' 
                    ){?>
                  <div class="message_contact  theme-notification"></div>
                  <form class="tg-formleavereview form-review">
                    <fieldset>
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="form-group">
                            <input type="text" name="user_subject" class="form-control" placeholder="<?php esc_attr_e('Subject','docdirect');?>">
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="tg-stars"><div id="jRate"></div><span class="your-rate"><strong><?php esc_html_e('Excellent','docdirect');?></strong></span></div>
                          <script type="text/javascript">
                        jQuery(function () {
                            var that = this;
                            var toolitup = jQuery("#jRate").jRate({
                                rating: 3,
                                min: 0,
                                max: 5,
                                precision: 1,
                                startColor: "<?php echo esc_js( $rating_color );?>",
                                endColor: "<?php echo esc_js( $rating_color );?>",
                                backgroundColor: "#DFDFE0",
                                onChange: function(rating) {
                                    jQuery('.user_rating').val(rating);
                                    if( rating == 1 ){
                                        jQuery('.your-rate strong').html(scripts_vars.rating_1);
                                    } else if( rating == 2 ){
                                        jQuery('.your-rate strong').html(scripts_vars.rating_2);
                                    } else if( rating == 3 ){
                                        jQuery('.your-rate strong').html(scripts_vars.rating_3);
                                    } else if( rating == 4 ){
                                        jQuery('.your-rate strong').html(scripts_vars.rating_4);
                                    } else if( rating == 5 ){
                                        jQuery('.your-rate strong').html(scripts_vars.rating_5);
                                    }
                                },
                                onSet: function(rating) {
                                    jQuery('.user_rating').val(rating);
                                    if( rating == 1 ){
                                        jQuery('.your-rate strong').html(scripts_vars.rating_1);
                                    } else if( rating == 2 ){
                                        jQuery('.your-rate strong').html(scripts_vars.rating_2);
                                    } else if( rating == 3 ){
                                        jQuery('.your-rate strong').html(scripts_vars.rating_3);
                                    } else if( rating == 4 ){
                                        jQuery('.your-rate strong').html(scripts_vars.rating_4);
                                    } else if( rating == 5 ){
                                        jQuery('.your-rate strong').html(scripts_vars.rating_5);
                                    }
                                }
                            });
                        });
                    </script>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group">
                            <textarea class="form-control" name="user_description" placeholder="<?php esc_attr_e('Review Description *','docdirect');?>"></textarea>
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <button class="tg-btn make-review" type="submit"><?php esc_html_e('Submit Review','docdirect');?></button>
                          <input type="hidden" name="user_rating" class="user_rating" value="" />
                          <input type="hidden" name="user_to" class="user_to" value="<?php echo esc_attr( $current_author_profile->ID );?>" />
                        </div>
                      </div>
                    </fieldset>
                  </form>
                  <?php } else{?>
                    <span><a href="javascript:;" class="tg-btn" data-toggle="modal" data-target=".tg-user-modal"><?php esc_html_e('Please Login To add Review','docdirect');?></a></span>
              <?php }?>
                </div>
                <?php }?>
              </div>
           <?php }?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } else{?>
	<div class="container">
         <?php DoctorDirectory_NotificationsHelper::informations(esc_html__('Oops! you are not allowed to access this page.','docdirect'));?>
    </div>
<?php }?> -->
<?php get_footer();?>
<?php
if( apply_filters('docdirect_is_setting_enabled',$current_author_profile->ID,'appointments' ) === true ){
	if( isset( $current_user->ID ) 
		&& 
		$current_user->ID != $current_author_profile->ID
		&&
		is_user_logged_in()
	){
	
		if( !empty( $privacy['appointments'] )
		  && 
			$privacy['appointments'] == 'on'
	 ) {
	
	?>
	<div class="modal fade tg-appointmentpopup" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	  <div class="modal-dialog modal-lg tg-modalcontent" role="document">
		<form action="#" method="post" class="appointment-form">
		  <fieldset class="booking-model-contents">
			<ul class="tg-navdocappointment" role="tablist">
			  <li class="active"><a href="javascript:;" class="bk-step-1"><?php esc_html_e('1. choose service','docdirect');?></a></li>
			  <li><a href="javascript:;" class="bk-step-2"><?php esc_html_e('2. available schedule','docdirect');?></a></li>
			  <li><a href="javascript:;" class="bk-step-3"><?php esc_html_e('3. your contact detail','docdirect');?></a></li>
			  <li><a href="javascript:;" class="bk-step-4"><?php esc_html_e('4. Payment Mode','docdirect');?></a></li>
			  <li><a href="javascript:;" class="bk-step-5"><?php esc_html_e('5. Finish','docdirect');?></a></li>
			</ul>
			<div class="tab-content tg-appointmenttabcontent" data-id="<?php echo esc_attr( $current_author_profile->ID );?>">
			  <div class="tab-pane active step-one-contents" id="one">
				<?php docdirect_get_booking_step_one($current_author_profile->ID,'echo');?>
			  </div>
			  <div class="tab-pane step-two-contents" id="two">
				<?php docdirect_get_booking_step_two_calender($current_author_profile->ID,'echo');?>
			  </div>
			  <div class="tab-pane step-three-contents" id="three"></div>
			  <div class="tab-pane step-four-contents" id="four"></div>
			  <div class="tab-pane step-five-contents" id="five"></div>
			  <div class="tg-btnbox booking-step-button">
				  <button type="button" class="tg-btn bk-step-prev"><?php esc_html_e('Previous','docdirect');?></button>
				  <button type="button" class="tg-btn bk-step-next"><?php esc_html_e('next','docdirect');?></button>
				</div>
			</div>
		  </fieldset>
		</form>
	  </div>
	</div>
<?php }}}?>