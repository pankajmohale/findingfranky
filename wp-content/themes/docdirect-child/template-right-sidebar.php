<?php
/* 
	Template Name: Left Sidebar Template
*/
get_header();
?>
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 aside sidebar-section" id="sidebar">
				<div class="aside sidebar-section" id="sidebar">
					<aside id="tg-sidebar" class="tg-sidebar tg-haslayout">
						<?php get_sidebar();?>
					</aside>
				</div>
			</div>
			<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
				<div class="franky-add-tips">
					<?php
						while ( have_posts() ) : the_post();
							the_content();
						endwhile; // End of the loop.
					?>					
				</div>
			</div>
		</div>
	</div>
<?php
get_footer();
?>
